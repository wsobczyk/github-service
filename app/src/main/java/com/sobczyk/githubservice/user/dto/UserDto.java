package com.sobczyk.githubservice.user.dto;

import lombok.*;

@Getter
@Builder
public class UserDto {
    private final int id;
    private final String login;
    private final String name;
    private final String type;
    private final String avatarUrl;
    private final String createdAt;
    private final double calculations;
}
