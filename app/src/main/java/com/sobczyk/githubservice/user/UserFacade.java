package com.sobczyk.githubservice.user;

import com.sobczyk.githubservice.DomainEventPublisher;
import com.sobczyk.githubservice.user.dto.UserDto;
import com.sobczyk.githubservice.user.vo.UserSearched;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class UserFacade {
    private final UserQueryRepository userQueryRepository;
    private final DomainEventPublisher publisher;

    public Optional<UserDto> search(String login) {
        Optional<User> userOpt = userQueryRepository.findByLogin(login);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            user.makeCalculations();
            publisher.publish(new UserSearched(user.getSnapshot().getLogin()));
            return Optional.of(toDto(user));
        }
        return Optional.empty();
    }

    private UserDto toDto(User user) {
        var snap = user.getSnapshot();
        return UserDto.builder()
                .id(snap.getId())
                .login(snap.getLogin())
                .name(snap.getName())
                .type(snap.getType())
                .avatarUrl(snap.getAvatarUrl())
                .createdAt(snap.getCreatedAt())
                .calculations(snap.getCalculations())
                .build();
    }
}
