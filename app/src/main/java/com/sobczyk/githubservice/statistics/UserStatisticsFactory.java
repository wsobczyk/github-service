package com.sobczyk.githubservice.statistics;

class UserStatisticsFactory {
    UserStatistics from (String login) {
        return UserStatistics.restore(new UserStatisticsSnapshot(login));
    }
}
