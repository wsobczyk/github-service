package com.sobczyk.githubservice.statistics;

import com.sobczyk.githubservice.user.vo.UserSearched;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserStatisticsFacade {
    private final UserStatisticsCommandRepository userStatisticsCommandRepository;
    private final UserStatisticsFactory userStatisticsFactory;

    public void handle(UserSearched userSearched) {
        userStatisticsCommandRepository.findByLogin(userSearched.getLogin())
                .ifPresentOrElse(userStatistics -> {
                            userStatistics.increment();
                            userStatisticsCommandRepository.save(userStatistics);
                        },
                        () -> {
                            UserStatistics userStatistics = userStatisticsFactory.from(userSearched.getLogin());
                            userStatistics.increment();
                            userStatisticsCommandRepository.save(userStatistics);
                        }
                );
    }
}
