FROM openjdk:15
ADD main/build/libs/main-1.0.0.jar main-1.0.0.jar
ENTRYPOINT ["java", "-jar", "main-1.0.0.jar"]