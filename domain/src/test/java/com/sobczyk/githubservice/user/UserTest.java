package com.sobczyk.githubservice.user;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class UserTest {
    @Test
    @Parameters({"6, 8, 10",
            "20, 4, 1.8",
            "3456, 255, 0.4461805555555556"})
    public void shouldMakeCalculations(int followers, int publicRepos, double expectedValue) {
        //given
        User user = new User(1,
                "test",
                "test",
                "test",
                "https://test.com",
                "22.01.2021",
                followers,
                publicRepos
        );

        //when
        user.makeCalculations();

        //then
        Assertions.assertEquals(expectedValue, user.getSnapshot().getCalculations(), 1);
    }

    @Test
    public void shouldReturnZeroWhenFollowersDoNotExist() {
        //given
        User user = new User(1,
                "test",
                "test",
                "test",
                "https://test.com",
                "22.01.2021",
                0,
                1000
        );
        int expectedValue = 0;

        //when
        user.makeCalculations();

        //then
        Assertions.assertEquals(expectedValue, user.getSnapshot().getCalculations(), 1);
    }
}