package com.sobczyk.githubservice;

import java.time.Instant;

public interface DomainEvent {
    Instant getOccurredOn();
}
