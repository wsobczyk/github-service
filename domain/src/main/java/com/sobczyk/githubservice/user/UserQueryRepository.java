package com.sobczyk.githubservice.user;

import java.util.Optional;

interface UserQueryRepository {
    Optional<User> findByLogin(String login);
}
