package com.sobczyk.githubservice.user;

class User {
    static User restore(UserSnapshot userSnapshot) {
        return new User(userSnapshot.getId(),
                userSnapshot.getLogin(),
                userSnapshot.getName(),
                userSnapshot.getType(),
                userSnapshot.getAvatarUrl(),
                userSnapshot.getCreatedAt(),
                userSnapshot.getFollowers(),
                userSnapshot.getPublicRepos()
        );
    }

    private final int id;
    private final String login;
    private final String name;
    private final String type;
    private final String avatarUrl;
    private final String createdAt;
    private final int followers;
    private final int publicRepos;
    private double calculations;

    User(int id, String login, String name, String type, String avatarUrl, String createdAt, int followers, int publicRepos) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.type = type;
        this.avatarUrl = avatarUrl;
        this.createdAt = createdAt;
        this.followers = followers;
        this.publicRepos = publicRepos;
    }

    void makeCalculations() {
        if (followers != 0) {
            calculations = 6d / followers * (2 + publicRepos);
        } else {
            calculations = 0;
        }
    }

    UserSnapshot getSnapshot() {
        return new UserSnapshot(id,
                login,
                name,
                type,
                avatarUrl,
                createdAt,
                followers,
                publicRepos,
                calculations
        );
    }
}
