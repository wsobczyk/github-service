package com.sobczyk.githubservice.user.vo;

import com.sobczyk.githubservice.DomainEvent;
import lombok.Getter;

import java.time.Instant;

@Getter
public class UserSearched implements DomainEvent {
    private final Instant occurredOn;
    private final String login;

    public UserSearched(String login) {
        occurredOn = Instant.now();
        this.login = login;
    }
}
