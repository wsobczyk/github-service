package com.sobczyk.githubservice.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
class UserSnapshot {
    private final int id;
    private final String login;
    private final String name;
    private final String type;
    private final String avatarUrl;
    private final String createdAt;
    private final int followers;
    private final int publicRepos;
    private final double calculations;
}
