package com.sobczyk.githubservice;

public interface DomainEventPublisher {
    void publish(DomainEvent event);
}
