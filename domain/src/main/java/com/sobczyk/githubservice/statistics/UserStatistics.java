package com.sobczyk.githubservice.statistics;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
class UserStatistics {
    static UserStatistics restore(UserStatisticsSnapshot userStatisticsSnapshot) {
        return new UserStatistics(userStatisticsSnapshot.getLogin(),
                userStatisticsSnapshot.getRequestCount()
        );
    }

    private final String login;
    private int requestCount;

    void increment() {
        requestCount++;
    }

    UserStatisticsSnapshot getSnapshot() {
        return new UserStatisticsSnapshot(login, requestCount);
    }
}
