package com.sobczyk.githubservice.statistics;

import java.util.Optional;

interface UserStatisticsCommandRepository {
    Optional<UserStatistics> findByLogin(String login);
    UserStatistics save (UserStatistics userStatistics);
}
