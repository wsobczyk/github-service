package com.sobczyk.githubservice.statistics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
class UserStatisticsSnapshot {
    private final String login;
    private final int requestCount;

    public UserStatisticsSnapshot(String login) {
        this.login = login;
        requestCount = 0;
    }
}
