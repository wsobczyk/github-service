package com.sobczyk.githubservice.statistics;

import com.sobczyk.githubservice.user.vo.UserSearched;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class UserStatisticsFacadeTest {
    private UserStatisticsFacade userStatisticsFacade;
    private UserStatisticsCommandRepository userStatisticsCommandRepository;

    @Test
    public void shouldIncrementCounter() {
        //given
        userStatisticsCommandRepository = new InMemoryUserStatisticsCommandRepository();
        userStatisticsFacade = new UserStatisticsFacade(userStatisticsCommandRepository, new UserStatisticsFactory());
        String login = "sobiech";
        UserSearched userSearched = new UserSearched(login);
        int expectedCounter = 16;

        //when
        userStatisticsFacade.handle(userSearched);

        //then
        Assertions.assertTrue(userStatisticsCommandRepository.findByLogin(login).isPresent());
        Assertions.assertEquals(expectedCounter, userStatisticsCommandRepository.findByLogin(login)
                .get()
                .getSnapshot()
                .getRequestCount()
        );
    }

    @Test
    public void shouldSetCounterToOneForTheNewUser() {
        //given
        userStatisticsCommandRepository = new InMemoryUserStatisticsCommandRepository();
        userStatisticsFacade = new UserStatisticsFacade(userStatisticsCommandRepository, new UserStatisticsFactory());
        String login = "example";
        UserSearched userSearched = new UserSearched(login);
        int expectedCounter = 1;

        //when
        userStatisticsFacade.handle(userSearched);

        //then
        Assertions.assertTrue(userStatisticsCommandRepository.findByLogin(login).isPresent());
        Assertions.assertEquals(expectedCounter, userStatisticsCommandRepository.findByLogin(login)
                .get()
                .getSnapshot()
                .getRequestCount()
        );
    }

    static class InMemoryUserStatisticsCommandRepository implements UserStatisticsCommandRepository {
        private Map<String, Integer> userStatisticsMap;

        public InMemoryUserStatisticsCommandRepository() {
            userStatisticsMap = new HashMap<>();
            initialize();
        }

        private void initialize() {
            userStatisticsMap.put("sobiech", 15);
            userStatisticsMap.put("andrzej", 100);
        }

        @Override
        public Optional<UserStatistics> findByLogin(String login) {
            Integer requestCount = userStatisticsMap.get(login);
            if (requestCount == null) {
                return Optional.empty();
            }
            return Optional.of(new UserStatistics(login, requestCount));
        }

        @Override
        public UserStatistics save(UserStatistics userStatistics) {
            userStatisticsMap.put(userStatistics.getSnapshot().getLogin(), userStatistics.getSnapshot().getRequestCount());
            return userStatistics;
        }
    }
}