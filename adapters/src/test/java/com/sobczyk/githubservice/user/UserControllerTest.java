package com.sobczyk.githubservice.user;

import com.sobczyk.githubservice.SpringDomainEventPublisher;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {UserController.class, UserFacade.class, SpringDomainEventPublisher.class})
@WebMvcTest
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserQueryRepository userQueryRepository;

    @Test
    public void shouldGetUser() throws Exception {
        //given
        String loginInRequest = "sobiech";
        String login = "Sobiech";
        int id = 10400526;
        String name = "KSO";
        String type = "User";
        String avatarUrl = "https://avatars.githubusercontent.com/u/10400526?v=4";
        String createdAt = "2015-01-05T12:08:02Z";
        int followers = 2;
        int publicRepos = 8;
        double expectedCalculations = 30;
        Mockito.when(userQueryRepository.findByLogin(loginInRequest)).thenReturn(Optional.of(new User(id,
                login,
                name,
                type,
                avatarUrl,
                createdAt,
                followers,
                publicRepos))
        );

        //when_then
        mockMvc.perform(get("/users/{loginInRequest}", loginInRequest)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)))
                .andExpect(jsonPath("$.login", is(login)))
                .andExpect(jsonPath("$.name", is(name)))
                .andExpect(jsonPath("$.type", is(type)))
                .andExpect(jsonPath("$.avatarUrl", is(avatarUrl)))
                .andExpect(jsonPath("$.createdAt", is(createdAt)))
                .andExpect(jsonPath("$.calculations", is(expectedCalculations)));
    }

   @Test
   public void shouldReturnNotFound() throws Exception {
       //given
       String loginInRequest = "asfascewcasas";
       Mockito.when(userQueryRepository.findByLogin(loginInRequest)).thenReturn(Optional.empty());

       //when_then
       mockMvc.perform(get("/users/{loginInRequest}", loginInRequest)
               .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isNotFound());
   }
}