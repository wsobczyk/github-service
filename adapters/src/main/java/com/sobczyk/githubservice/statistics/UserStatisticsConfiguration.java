package com.sobczyk.githubservice.statistics;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class UserStatisticsConfiguration {
    private final UserStatisticsCommandRepository userStatisticsCommandRepository;

    @Bean
    public UserStatisticsFacade userStatisticsFacade() {
        return new UserStatisticsFacade(userStatisticsCommandRepository, userStatisticsFactory());
    }

    @Bean
    public UserStatisticsFactory userStatisticsFactory() {
        return new UserStatisticsFactory();
    }
}
