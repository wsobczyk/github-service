package com.sobczyk.githubservice.statistics;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
class UserStatisticsCommandRepositoryImpl implements UserStatisticsCommandRepository {
    private final SqlUserStatisticsCommandRepository sqlUserStatisticsCommandRepository;

    @Override
    public Optional<UserStatistics> findByLogin(String login) {
        return sqlUserStatisticsCommandRepository.findByLogin(login)
                .map(SqlUserStatistics::toSnapshot)
                .map(UserStatistics::restore);
    }

    @Override
    public UserStatistics save(UserStatistics userStatistics) {
        return UserStatistics.restore(
                sqlUserStatisticsCommandRepository.save(
                        SqlUserStatistics.from(userStatistics.getSnapshot()))
                        .toSnapshot()
        );
    }
}

@Repository
interface SqlUserStatisticsCommandRepository extends JpaRepository<SqlUserStatistics, Long> {
    Optional<SqlUserStatistics> findByLogin(String login);
}
