package com.sobczyk.githubservice.statistics;

import com.sobczyk.githubservice.user.vo.UserSearched;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class UserStatisticsEventListener {
    private final UserStatisticsFacade userStatisticsFacade;

    @EventListener
    public void on(UserSearched userSearched) {
        userStatisticsFacade.handle(userSearched);
    }
}
