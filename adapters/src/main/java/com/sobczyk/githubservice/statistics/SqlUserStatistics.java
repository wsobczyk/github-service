package com.sobczyk.githubservice.statistics;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_statistics")
@NoArgsConstructor
@AllArgsConstructor
class SqlUserStatistics {
    static SqlUserStatistics from(UserStatisticsSnapshot userStatisticsSnapshot) {
        return new SqlUserStatistics(userStatisticsSnapshot.getLogin(), userStatisticsSnapshot.getRequestCount());
    }

    @Id
    private String login;

    private int requestCount;

    UserStatisticsSnapshot toSnapshot() {
        return new UserStatisticsSnapshot(login, requestCount);
    }
}
