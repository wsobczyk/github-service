package com.sobczyk.githubservice.user;

import com.sobczyk.githubservice.DomainEvent;
import com.sobczyk.githubservice.DomainEventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@RequiredArgsConstructor
class UserConfiguration {
    private final DomainEventPublisher publisher;
    @Value("${github.api.url}")
    private String urlTemplate;

    @Bean
    public UserFacade userFacade() {
        return new UserFacade(userQueryRepository(), publisher);
    }

    @Bean
    public UserQueryRepository userQueryRepository() {
        return new UserQueryRepositoryImpl(new RestTemplate(), urlTemplate);
    }
}
