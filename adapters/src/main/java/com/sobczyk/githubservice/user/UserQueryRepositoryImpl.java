package com.sobczyk.githubservice.user;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@RequiredArgsConstructor
class UserQueryRepositoryImpl implements UserQueryRepository {
    private final RestTemplate restTemplate;
    @Value("${github.api.url}")
    private final String urlTemplate;

    @Override
    public Optional<User> findByLogin(String login) {
        String url = urlTemplate.replace("{login}", login);
        try {
            return Optional.ofNullable(restTemplate.getForObject(url, GithubApiUser.class))
                    .map(githubApiUser -> new UserSnapshot(githubApiUser.getId(),
                            githubApiUser.getLogin(),
                            githubApiUser.getName(),
                            githubApiUser.getType(),
                            githubApiUser.getAvatar_url(),
                            githubApiUser.getCreated_at(),
                            githubApiUser.getFollowers(),
                            githubApiUser.getPublic_repos(),
                            0)
                    ).map(User::restore);
        } catch (Exception exception) {
            return Optional.empty();
        }
    }
}
