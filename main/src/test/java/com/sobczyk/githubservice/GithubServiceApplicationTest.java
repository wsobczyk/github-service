package com.sobczyk.githubservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = GithubServiceApplication.class)
class GithubServiceApplicationTest {

    @Test
    public void contextLoad() {

    }
}